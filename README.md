# README #

ToolExchange App written in React Native (iOS and Android). 

### What is this repository for? ###

* ToolExchange is an idea for an app to allow groups of friends to share tools with one another
* 0.0.1

### How do I get set up? ###

* Follow instructions for getting React Native setup locally https://facebook.github.io/react-native/docs/getting-started.html
* yarn upgrade
* react-native run-ios (or run-android)



# CODE-PUSH #
 Production │ 4M8rqGkvY07KyKkxKLzv9vri0wIoVJtbS-4YG 
 Staging    │ wrLrRuZOh4beIKUKViBWzW0SYbFxVJtbS-4YG

code-push release-react ToolExchange ios -d Production
code-push deployment ls ToolExchange -k

# Prod build on Android #
cd android && ./gradlew assembleRelease
react-native run-android --variant=release

# Prod build on iOS #
Open xcode
change build schema to release
press play with device as destination