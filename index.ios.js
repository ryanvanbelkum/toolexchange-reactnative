import React, { Component } from 'react';
import codePush from 'react-native-code-push';
import { AppRegistry } from 'react-native';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import createLogger from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import reducer from './src/root-reducer';
import App from './src/app';

const loggerMiddleware = createLogger();
const store = createStore(
    reducer,
    applyMiddleware(
        thunkMiddleware,
        loggerMiddleware,
    ),
);

class ToolExchangeClass extends Component {

  constructor() {
    super();
    console.disableYellowBox = true;
  }

  componentDidMount() {
    codePush.notifyAppReady();
  }

  render() {
    return (
      <Provider store={store} >
        <App />
      </Provider >
    );
  }
}

const ToolExchange = codePush(ToolExchangeClass);

AppRegistry.registerComponent('ToolExchange', () => ToolExchange);
