import { Platform, Linking, View, Image, TouchableOpacity, Text, Modal, ScrollView } from 'react-native';
import styles from './tool-info-style.js';
import React, { Component } from 'react';
import Hr from 'react-native-hr';
import BorrowReturnButton from '../borrow-return-button/borrow-return-button.js';
import ToolHeroImage from '../tool-hero-image/tool-hero-image.js';

const close = require('../../img/Cancel-58.png');
const phone = require('../../img/Phone-58.png');
const map = require('../../img/map.png');

function openDefaultMaps(address) {
  let url = Platform.OS === 'ios' ? `http://maps.apple.com/?daddr=${address.street} ${address.city} ${address.zip}&dirflg=d` :
    `geo:0,0?q=${address.street} ${address.city} ${address.zip}`;
  Linking.openURL(url).catch(err => console.error('An error occurred', err));
}

function makePhoneCall(tel) {
  Linking.openURL(`tel:${tel}`).catch(err => console.error('An error occurred', err));
}

const ToolInfo = ({ description, image, name, borrower, address, owner, toolKey, borrowTool, returnTool, currentUser, openToolInfo, toggleToolInfo, telephone }) => {

  const borrowerText = borrower || 'AVAILABLE';
  const phoneIcon = telephone ? (<TouchableOpacity onPress={makePhoneCall.bind(this, telephone)}>
    <Image style={styles.phone} source={phone} />
  </TouchableOpacity>) : null;


  return (
    <Modal
      style={styles.container}
      animationType={'slide'}
      transparent={false}
      visible={openToolInfo}
      onRequestClose={() => {}}
    >
      <TouchableOpacity style={styles.closeButton} onPress={toggleToolInfo}>
        <Image style={styles.closeIcon} source={close} />
      </TouchableOpacity>
      <ScrollView style={styles.scrollView} automaticallyAdjustContentInsets={false} scrollEventThrottle={200}>
        <ToolHeroImage name={name} image={image} />
        <View style={styles.content}>
          <View style={styles.infoSubFooter} >
            <View style={styles.subFooterLeft}>
              <View style={styles.lineItem}>
                <Text style={styles.text}>
                  Owner: { owner } -
                </Text>
                { phoneIcon }
              </View>
              <View style={styles.lineItem}>
                <Text style={styles.text}>
                  borrower: { borrowerText }
                </Text>
              </View>
            </View>
            <View style={styles.subFooterRight}>
              <TouchableOpacity style={styles.mapTouch} onPress={openDefaultMaps.bind(this, address)}>
                <View style={styles.mapView}>
                  <Image style={styles.mapImage} source={map} />
                </View>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.borrowCard} >
            <BorrowReturnButton borrower={borrower} currentUser={currentUser} borrowTool={borrowTool} returnTool={returnTool} owner={owner} />
          </View>
          <Hr lineColor="#b3b3b3" text="description" />
          <View style={styles.descriptionView}>
            <Text style={styles.toolDescription}>
              { description }
            </Text>
          </View>
        </View>
      </ScrollView>
    </Modal>
  );
};


ToolInfo.propTypes = {
  description: React.PropTypes.string,
  image: React.PropTypes.string,
  telephone: React.PropTypes.string,
  name: React.PropTypes.string,
  owner: React.PropTypes.string,
  borrower: React.PropTypes.string,
  toolKey: React.PropTypes.string,
  borrowTool: React.PropTypes.func,
  returnTool: React.PropTypes.func,
  openToolInfo: React.PropTypes.bool,
  toggleToolInfo: React.PropTypes.func
}

export default ToolInfo;
