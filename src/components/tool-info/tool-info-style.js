import { StyleSheet, Dimensions } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
  },
  scrollView: {
    backgroundColor: '#F0EEED'
  },
  map: {
    height: 70,
    width: 70,
  },
  mapView: {
    padding: 5
  },
  mapTouch: {
    alignSelf: 'flex-end'
  },
  mapImage: {
    height: 70,
    width: 70,
    borderRadius: 35,
  },
  borrowerIcon: {
    height: 30,
    width: 30,
    marginRight: 10
  },
  lineItem: {
    flexDirection: 'row',
  },
  closeIcon: {
    zIndex: 100,
    width: 30,
    height: 30
  },
  closeButton: {
    position: 'absolute',
    right: 10,
    top: 15,
    zIndex: 100,
    backgroundColor: 'white',
    borderRadius: 35,
  },
  phone: {
    width: 25,
    height: 25,
  },
  toolDescription: {
    fontSize: 16,
    paddingBottom: 10,
    paddingTop: 10,
    paddingLeft: 3
  },
  text: {
    fontSize: 16,
    padding: 3
  },
  telephone: {
    color: 'blue'
  },
  infoSubFooter: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: 10,
    backgroundColor: 'white',
    alignItems: 'center'
  },
  borrowCard: {
    backgroundColor: 'white',
    alignItems: 'center',
    margin: 10,
  },
  subFooterRight: {
    alignSelf: 'flex-end'
  },
  descriptionView: {
    margin: 10,
    backgroundColor: 'white',
  }
});

export default styles;
