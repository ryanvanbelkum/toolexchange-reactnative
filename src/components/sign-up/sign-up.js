import { View, Text, Modal, ScrollView, TouchableOpacity, Alert, Image } from 'react-native';
import React, { PureComponent } from 'react';
import Rebase from 're-base';
import { Form, Separator, InputField } from 'react-native-form-generator';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import styles from './sign-up-style';
import { rebase } from '../../helpers';

const base = Rebase.createClass(rebase);
const close = require('../../img/Cancel-58.png');

class SignUp extends PureComponent {

  constructor() {
    super();
    this.updateFirstName = this.handleInputChange.bind(this, 'firstName');
    this.updateLastName = this.handleInputChange.bind(this, 'lastName');
    this.updateUserName = this.handleInputChange.bind(this, 'userName');
    this.updateEmail = this.handleInputChange.bind(this, 'email');
    this.updateStreetAddress = this.handleInputChange.bind(this, 'streetAddress');
    this.updateCityAddress = this.handleInputChange.bind(this, 'cityAddress');
    this.updateStateAddress = this.handleInputChange.bind(this, 'stateAddress');
    this.updateZipAddress = this.handleInputChange.bind(this, 'zipAddress');
    this.updateTelephone = this.handleInputChange.bind(this, 'telephone');
    this.updatePassword = this.handleInputChange.bind(this, 'password');
    this.updatePassword2 = this.handleInputChange.bind(this, 'password2');
    this.submitNewUser = this.submitNewUser.bind(this);
    this.checkForUsername = this.checkForUsername.bind(this);

    this.state = {
      userIndex: [],
      userNameValid: true,
      passwordsValid: true,
    };
  }

  componentDidMount() {
    if (this.firebaseRef) {
      this.componentWillUnmount();
    }
    this.firebaseRef = base.syncState('userIndex', {
      context: this,
      state: 'userIndex',
      asArray: true,
    });
  }

  componentWillUnmount() {
    base.removeBinding(this.firebaseRef);
  }

  handleInputChange(value, text) {
    this.setState({
      [value]: text,
    });
  }

  checkForUsername() {
    const username = this.state.userName;
    const matchedUser = this.state.userIndex.filter(user => user === username);
    if (matchedUser.length > 0) {
      Alert.alert(
        'Username already exists',
        'Please choose another username',
        [
          {
            text: 'OK',
          },
        ],
        {
          cancelable: false,
        },
      );
      this.setState({
        userNameValid: false,
      });
    } else {
      this.setState({
        userNameValid: true,
      });
    }
  }

  submitNewUser() {
    const { firstName, lastName, userName, telephone, email, password, password2, streetAddress, cityAddress, stateAddress, zipAddress, userNameValid } = this.state;
    const passwordsValid = password === password2 && password !== undefined && password2 !== undefined;
    const { createNewUser, successCallback } = this.props;

    if (passwordsValid && userNameValid) {
      const address = {
        street: streetAddress,
        city: cityAddress,
        state: stateAddress,
        zip: zipAddress,
      };
      createNewUser(firstName, lastName, userName, telephone, email, address, password, successCallback);
    } else {
      Alert.alert(
        'Either your passwords do not match, or your username is already taken',
        'Please correct these fields',
        [
          {
            text: 'OK',
          },
        ],
        {
          cancelable: false,
        },
      );
    }
    this.setState({
      passwordsValid,
    });
  }

  render() {
    const userNameStyle = !this.state.userNameValid ? styles.errorInput : null;
    const passwordStyle = !this.state.passwordsValid ? styles.errorInput : null;

    return (
      <Modal
        animationType={'slide'}
        transparent={false}
        visible={this.props.showModal}
        onRequestClose={() => {}}
      >
        <ScrollView style={styles.scrollView} keyboardShouldPersistTaps="always">
          <KeyboardAwareScrollView>
            <Form>
              <View>
                <TouchableOpacity style={styles.closeButton} onPress={this.props.toggleModal}>
                  <Image style={styles.closeIcon} source={close} />
                </TouchableOpacity>
                <Separator label="Sign-up Information" />
              </View>
              <InputField onValueChange={this.updateFirstName} placeholder="First Name" />
              <InputField onValueChange={this.updateLastName} placeholder="Last Name" />
              <InputField
                autoCapitalize="none"
                style={userNameStyle}
                onValueChange={this.updateUserName}
                placeholder="Username"
                onBlur={this.checkForUsername}
              />
              <InputField autoCapitalize="none" onValueChange={this.updateEmail} placeholder="Email Address" />
              <InputField onValueChange={this.updateStreetAddress} placeholder="Street" />
              <InputField onValueChange={this.updateCityAddress} placeholder="City" />
              <InputField onValueChange={this.updateStateAddress} placeholder="State (Ex. MO)" />
              <InputField onValueChange={this.updateZipAddress} placeholder="Zip" />
              <InputField onValueChange={this.updateTelephone} placeholder="Telephone" />
              <InputField
                autoCapitalize="none"
                style={passwordStyle}
                secureTextEntry
                onValueChange={this.updatePassword}
                placeholder="Password"
              />
              <InputField
                autoCapitalize="none"
                style={passwordStyle}
                secureTextEntry
                onValueChange={this.updatePassword2}
                placeholder="Repeat Password"
              />
              <TouchableOpacity style={styles.submitButton} onPress={this.submitNewUser}>
                <Text style={styles.submitButtonText}>
                  SIGN UP
                </Text>
              </TouchableOpacity>
            </Form>
          </KeyboardAwareScrollView>
        </ScrollView>
      </Modal>
    );
  }
}


SignUp.propTypes = {
  showModal: React.PropTypes.bool.isRequired,
  toggleModal: React.PropTypes.func,
  createNewUser: React.PropTypes.func,
  successCallback: React.PropTypes.func,
};

export default SignUp;
