import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  seperator: {
    backgroundColor: '#DCDCDC',
    flex: 1
  },
  submitButton: {
    height: 50,
    backgroundColor: '#1f6389',
    justifyContent: 'center',
    alignItems: 'center'
  },
  submitButtonText: {
    color: 'white'
  },
  errorInput: {
    backgroundColor: '#ff7f7f'
  },
  closeIcon: {
    zIndex: 100,
    width: 30,
    height: 30
  },
  closeButton: {
    position: 'absolute',
    right: 10,
    top: 20,
    zIndex: 100
  },
  scrollView: {
    flex: 1
  }
});

export default styles;


