/* eslint class-methods-use-this: ["error", { "exceptMethods": ["getToolOfFriends", "getToolsDatasource"] }] */

import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import Rebase from 're-base';
import Hr from 'react-native-hr';
import randomString from 'random-string';
import { SwipeListView } from 'react-native-swipe-list-view';
import { View, Text, ListView, ScrollView, TouchableOpacity, Image } from 'react-native';
import styles from './profile-style';
import { tabs, rebase } from '../../helpers';
import Tool from '../tool/tool';
import AddNewTool from '../add-new-tool/add-new-tool';

const base = Rebase.createClass(rebase);
const garageHeader = require('../../img/garage-header.jpg');
const personIcon = require('../../img/person-outline.png');
const emailIcon = require('../../img/email-50.png');
const addressIcon = require('../../img/home-50.png');
const phoneIcon = require('../../img/phone-50.png');

class ProfileClass extends PureComponent {

  constructor() {
    super();

    this.state = {
      user: {
        tools: {},
        borrowedTools: {},
        profile: {
          firstName: '',
          lastName: '',
          address: {},
        },
      },
      firebaseAuthenticated: false,
      showNewToolModal: false,
    };

    this.toggleNewToolModal = this.toggleNewToolModal.bind(this);
    this.addNewTool = this.addNewTool.bind(this);
  }

  componentWillUpdate(nextProps) {
    if (this.props.username !== nextProps.username) {
      this.syncWithFirebase(nextProps.username);
    }
  }

  componentWillUnmount() {
    base.removeBinding(this.firebaseRef);
  }

  toggleNewToolModal() {
    this.setState({
      showNewToolModal: !this.state.showNewToolModal,
    });
  }

  addNewTool(name, description, image) {
    if (name) {
      const id = randomString({
        length: 10,
      });
      const toolObj = {
        toolKey: id,
        name,
        owner: this.props.username,
        description,
        image,
      };
      if (!this.state.user.tools) {
        this.state.user.tools = {};
      }
      this.state.user.tools[id] = toolObj;

      this.setState({
        user: this.state.user,
      });
      this.toggleNewToolModal();
    }
  }

  getOwnTools(tools) {
    return (<SwipeListView
      dataSource={tools}
      renderRow={rowData => this.renderRow(rowData)}
      renderHiddenRow={rowData => this.renderHiddenRow(rowData)}
      leftOpenValue={75}
      rightOpenValue={-75}
      closeOnRowPress
      disableRightSwipe
      enableEmptySections
    />);
  }

  syncWithFirebase(username) {
    if (this.firebaseRef) {
      this.componentWillUnmount();
    }
    this.firebaseRef = base.syncState(`garages/${username}`, {
      context: this,
      state: 'user',
      asArray: false,
      then: () => {
        this.setState({
          firebaseAuthenticated: true,
        });
      },
      onFailure: (data) => {
        if (data.code === 'PERMISSION_DENIED') {
          this.setState({
            firebaseAuthenticated: false,
          });
        }
      },
    });
  }

  get toolStats() {
    const toolsOwned = Object.values(this.state.user.tools || {});
    const toolsBorrowed = Object.values(this.state.user.borrowedTools || {});
    return { toolsOwned, toolsBorrowed };
  }

  getToolsDatasource(tools) {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2,
    });
    return ds.cloneWithRows(tools);
  }

  renderRow(rowData) {
    return (
      <TouchableOpacity activeOpacity={1} onPress={() => {}}>
        <View>
          <Tool
            name={rowData.name}
            description={rowData.description}
            image={rowData.image}
            tab={''}
            borrower={rowData.borrower}
            owner={rowData.owner}
            currentUser={this.props.username}
          />
        </View>
      </TouchableOpacity>
    );
  }

  get addNewToolComponent() {
    return this.props.currentTab === tabs.PROFILE ?
      <View style={styles.addNewTool}>
        <AddNewTool addNewTool={this.addNewTool} toggleModal={this.toggleNewToolModal} modalOpen={this.state.showNewToolModal} />
      </View> : null;
  }

  renderHiddenRow(rowData) {
    const borrowedTool = rowData.borrower === this.props.username;
    const text = borrowedTool ? 'Return' : 'Delete';
    const onPress = borrowedTool ? this.returnTool.bind(this, rowData) : this.deleteTool.bind(this, rowData);
    return (
      <TouchableOpacity activeOpacity={1} style={styles.rowBackView} onPress={onPress}>
        <Text style={styles.rowBackText}>
          { text }
        </Text>
      </TouchableOpacity>
    );
  }

  deleteTool(tool) {
    this.state.user.tools[tool.toolKey] = null;
    this.setState({
      user: this.state.user,
    });
  }

  returnTool(tool) {
    this.state.user.borrowedTools[tool.toolKey] = null;

    this.setState({
      user: this.state.user,
    });
    base.remove(`garages/${tool.owner}/tools/${tool.toolKey}/borrower`, () => {});
  }

  render() {
    const { toolsOwned, toolsBorrowed } = this.toolStats;
    const address = this.state.user.profile.address;
    const toolView = this.getOwnTools(this.getToolsDatasource({
      ...this.state.user.tools,
      ...this.state.user.borrowedTools,
    }));

    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.header}>
            <Image style={styles.garageHeader} source={garageHeader} />
            <Image style={styles.profilePic} source={personIcon} />
          </View>
          <View style={styles.body}>
            <Text style={styles.username}>
              {this.state.user.profile.firstName} {this.state.user.profile.lastName}
            </Text>
            <Text style={styles.stats}>
              Tools owned: {toolsOwned.length}
            </Text>
            <Text style={styles.stats}>
              Tools borrowing: {toolsBorrowed.length}
            </Text>
            <View style={styles.userInfo}>
              <View style={styles.lineItem}>
                <Image style={styles.infoIcon} source={addressIcon} />
                <Text style={styles.userInfoText}>
                  {address.street} {address.city}, {address.state} {address.zip}
                </Text>
              </View>
              <View style={styles.lineItem}>
                <Image style={styles.infoIcon} source={emailIcon} />
                <Text style={styles.userInfoText}>
                  {this.state.user.profile.email}
                </Text>
              </View>
              <View style={styles.lineItem}>
                <Image style={styles.infoIcon} source={phoneIcon} />
                <Text style={styles.userInfoText}>
                  {this.state.user.profile.telephone}
                </Text>
              </View>
            </View>
            <Hr lineColor="#b3b3b3" />
            <View style={styles.toolView}>
              {toolView}
            </View>
          </View>
        </ScrollView>
        { this.addNewToolComponent }
      </View>);
  }
}

ProfileClass.propTypes = {
  username: React.PropTypes.string.isRequired,
};

function mapStateToProps(state) {
  const login = state.login;
  return {
    username: login.username,
    currentTab: state.navigation.currentTab,
  };
}

const Profile = connect(
  mapStateToProps,
)(ProfileClass);

export default Profile;
