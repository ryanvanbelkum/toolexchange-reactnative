import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({

  container: {
    flex: 1,
  },
  header: {
    flex: 1,
  },
  body: {
    flex: 2,
    paddingTop: 55,
  },
  username: {
    fontSize: 22,
    alignSelf: 'center',
    paddingBottom: 10,
  },
  userInfo: {
    marginLeft: 10,
    marginTop: 10,
  },
  userInfoText: {
    marginLeft: 5,
    fontSize: 16,
  },
  lineItem: {
    flexDirection: 'row',
  },
  infoIcon: {
    marginTop: 3,
    width: 15,
    height: 15,
  },
  stats: {
    fontSize: 16,
    alignSelf: 'center',
  },
  profilePic: {
    height: 100,
    width: 100,
    backgroundColor: '#FFF',
    borderRadius: 50,
    position: 'absolute',
    top: 140,
    alignSelf: 'center',
  },
  garageHeader: {
    width: null,
    height: 200,
    resizeMode: 'cover',
  },
  toolView: {
    marginTop: 20,
  },
  rowBackView: {
    flex: 1,
    backgroundColor: 'red',
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingRight: 15,
  },
  addNewTool: {
    height: 50,
    backgroundColor: '#F0EEED',
    justifyContent: 'flex-end',
    alignSelf: 'stretch',
  },
});


export default styles;
