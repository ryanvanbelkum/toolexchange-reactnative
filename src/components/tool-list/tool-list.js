/* eslint class-methods-use-this: ["error", { "exceptMethods": ["getToolOfFriends", "getToolsDatasource"] }] */

import React, { PureComponent } from 'react';
import { map, isEmpty } from 'lodash';
import randomString from 'random-string';
import { SwipeListView } from 'react-native-swipe-list-view';
import Rebase from 're-base';
import { connect } from 'react-redux';
import { View, Text, ListView, TouchableOpacity, InteractionManager } from 'react-native';
import styles from './tool-list-style';
import { hideActivityIndicator } from '../navigation/navigation-actions';
import { storeUserTree } from './tool-list-actions';
import { tabs, rebase } from '../../helpers';
import FriendsList from '../friends-list/friends-list';
import ToolCard from '../tool-card/tool-card';
import Tool from '../tool/tool';
import AddNewTool from '../add-new-tool/add-new-tool';
import ToolInfo from '../tool-info/tool-info';
import pushService from '../../push-service';

const base = Rebase.createClass(rebase);

class ToolListClass extends PureComponent {

  constructor() {
    super();

    this.state = {
      users: {},
      friendsListOpen: false,
      showNewToolModal: false,
      openToolInfo: false,
      firebaseAuthenticated: false,
    };
    this.toggleNewToolModal = this.toggleNewToolModal.bind(this);
    this.updateFriendsList = this.updateFriendsList.bind(this);
    this.toggleFriendsList = this.toggleFriendsList.bind(this);
    this.toggleToolInfo = this.toggleToolInfo.bind(this);
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      if (this.firebaseRef) {
        this.componentWillUnmount();
      }
      this.setState({
        users: this.props.users,
      });
      this.firebaseRef = base.syncState('garages', {
        context: this,
        state: 'users',
        asArray: false,
        then: () => {
          this.handleInputChange('firebaseAuthenticated', true);
        },
        onFailure: (data) => {
          if (data.code === 'PERMISSION_DENIED') {
            this.handleInputChange('firebaseAuthenticated', false);
          }
        },
      });
    });
  }

  componentWillUpdate(nextProps, nextState) {
    if (!nextState.firebaseAuthenticated && this.props.loggedInUsername) {
      this.componentDidMount();
    }
    if (!isEmpty(nextState.users) && nextProps.showActivityIndicator) {
      this.props.hideActivityIndicator();
    }
  }

  componentWillUnmount() {
    this.props.storeUserTree(this.state.users);
    base.removeBinding(this.firebaseRef);
  }

  getToolOfFriends(users, user) {
    const arr = [];
    if (users[user] && users[user].friends) {
      Object.keys(users[user].friends).forEach((friend) => {
        arr.push(users[friend]);
      });
    }
    return arr;
  }

  getToolCard(tools) {
    let toolArr = map(tools, 'tools');
    toolArr = toolArr.filter(tool => tool !== undefined);
    const ds = this.getToolsDatasource(toolArr);
    return toolArr.length > 0 ?
      <ListView dataSource={ds} enableEmptySections renderRow={rowData => this.renderToolCardRow(rowData)} /> :
      <View style={styles.centering}>
        <Text style={styles.noFriends}>
          You have no friends :(
        </Text>
      </View>;
  }

  getToolsDatasource(tools) {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2,
    });
    return ds.cloneWithRows(tools);
  }

  toggleNewToolModal() {
    this.setState({
      showNewToolModal: !this.state.showNewToolModal,
    });
  }

  toggleToolInfo(tool) {
    this.setState({
      openToolInfo: !this.state.openToolInfo,
      currentToolInfo: tool,
    });
  }

  closeToolInfo() {
    this.setState({
      openToolInfo: false,
    });
  }

  toggleFriendsList() {
    this.setState({
      friendsListOpen: !this.state.friendsListOpen,
    });
  }

  handleInputChange(value, text) {
    this.setState({
      [value]: text,
    });
  }

  borrowTool(tool) {
    const borrower = this.props.loggedInUsername;
    this.state.users[tool.owner].tools[tool.toolKey] = {
      ...tool,
      borrower,
    };
    if (!this.state.users[borrower].borrowedTools) {
      this.state.users[borrower].borrowedTools = {};
    }
    this.state.users[borrower].borrowedTools[tool.toolKey] = {
      ...tool,
      borrower,
    };

    this.setState({
      users: this.state.users,
    });

    if (this.state.users[tool.owner].profile.token) {
      pushService.sendNotification(this.state.users[tool.owner].profile.token, `Your ${tool.name} is being borrowed`, `${borrower} is requesting your tool`);
    }
    this.toggleToolInfo();
  }

  returnTool(tool) {
    const borrower = null;
    this.state.users[tool.owner].tools[tool.toolKey] = {
      ...tool,
      borrower,
    };
    this.state.users[tool.borrower].borrowedTools[tool.toolKey] = null;

    this.setState({
      users: this.state.users,
    });

    if (this.state.users[tool.owner].profile.token) {
      pushService.sendNotification(this.state.users[tool.owner].profile.token, `Your ${tool.name} is being returned`, `${tool.borrower} is returning your tool`);
    }
    this.closeToolInfo();
  }

  updateFriendsList(friend) {
    const loggedInUser = this.props.loggedInUsername;
    if (!this.state.users[loggedInUser].friends) {
      this.state.users[loggedInUser].friends = [];
    }

    const user = this.state.users[loggedInUser].friends[friend.username];
    this.state.users[loggedInUser].friends[friend.username] = !user ? friend : null;

    this.setState({
      users: this.state.users,
    });
  }

  get toolInfoModal() {
    const currentTool = this.state.currentToolInfo;
    const borrowCurrentTool = this.borrowTool.bind(this, currentTool);
    const returnCurrentTool = this.returnTool.bind(this, currentTool);

    return currentTool ?
      <ToolInfo
        image={currentTool.image}
        description={currentTool.description}
        name={currentTool.name}
        borrower={currentTool.borrower}
        owner={currentTool.owner}
        telephone={this.props.telephone}
        address={this.props.loggedInUserAddress}
        toolKey={currentTool.toolKey}
        borrowTool={borrowCurrentTool}
        returnTool={returnCurrentTool}
        currentUser={this.props.loggedInUsername}
        openToolInfo={this.state.openToolInfo}
        toggleToolInfo={this.toggleToolInfo}
      /> : null;
  }

  renderToolCardRow(rowData) {
    return <ToolCard tools={rowData} onPress={this.toggleToolInfo} />;
  }

  renderRow(rowData) {
    const toggleToolInfo = this.toggleToolInfo.bind(this, rowData);
    return (
      <TouchableOpacity activeOpacity={1} onPress={toggleToolInfo}>
        <View>
          <Tool
            name={rowData.name}
            description={rowData.description}
            image={rowData.image}
            tab={this.props.currentTab}
            borrower={rowData.borrower}
            owner={rowData.owner}
            currentUser={this.props.loggedInUsername}
          />
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    const { loggedInUsername, showActivityIndicator, title } = this.props;
    const users = this.state.users;
    const user = loggedInUsername;
    const friends = users[user] && users[user].friends ? Object.keys(users[user].friends) : [];
    const toolOfFriends = this.getToolOfFriends(users, user);
    const toolView = !showActivityIndicator ? this.getToolCard(toolOfFriends) : null;

    return (
      <View style={styles.container}>
        { this.toolInfoModal }
        <View style={[styles.box, styles.box1]}>
          <Text style={styles.headerText}>
            {title}
          </Text>
          <View style={styles.friendList}>
            <FriendsList
              loggedInUser={loggedInUsername}
              users={map(users, 'profile')}
              friends={friends}
              updateFriendsList={this.updateFriendsList}
              toggleFriendsList={this.toggleFriendsList}
              friendsListOpen={this.state.friendsListOpen}
            />
          </View>
        </View>
        <View style={[styles.box, styles.box2]}>
          { toolView }
        </View>
      </View>
    );
  }
}

ToolListClass.defaultProps = {
  loggedInUsername: '',
  telephone: '',
  loggedInUserAddress: '',
  showActivityIndicator: false,
  currentTab: tabs.OWN_TOOLS,
  users: '',
  title: '',
};

ToolListClass.propTypes = {
  loggedInUsername: React.PropTypes.string,
  telephone: React.PropTypes.string,
  loggedInUserAddress: React.PropTypes.shape({}),
  showActivityIndicator: React.PropTypes.bool,
  currentTab: React.PropTypes.string,
  users: React.PropTypes.shape({}),
  title: React.PropTypes.string,
  hideActivityIndicator: React.PropTypes.func.isRequired,
  storeUserTree: React.PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    loggedInUsername: state.login.loggedInUsername,
    token: state.login.token,
    telephone: state.login.telephone,
    loggedInUserAddress: state.login.address,
    showActivityIndicator: state.navigation.showActivityIndicator,
    currentTab: state.navigation.currentTab,
    users: state.toolList.users,
    title: state.navigation.title,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    hideActivityIndicator() {
      dispatch(hideActivityIndicator());
    },
    storeUserTree(users) {
      dispatch(storeUserTree(users));
    },
  };
}

const ToolList = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ToolListClass);

export default ToolList;
