const defaultState = {
  users: {},
};

export default function toolList(state = defaultState, action) {
  switch (action.type) {
    case 'STORE_USER_TREE':
      return {
        ...state,
        users: action.users
      };
    default:
      return state;
  }
}