export function storeUserTree(users) {
  return {
    type: 'STORE_USER_TREE',
    users: users
  };
}
