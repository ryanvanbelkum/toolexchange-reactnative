import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  box: {

  },
  box1: {
    height: 60,
    backgroundColor: '#1f6389',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  headerText: {
    color: '#FFF',
    fontSize: 20,
    fontFamily: 'Iowan Old Style',
  },
  textInput: {
    height: 45,
    textAlign: 'center',
  },
  textInputView: {
    backgroundColor: '#FFF',
  },
  box2: {
    flex: 1,
    backgroundColor: '#F0EEED',
  },
  box3: {
    height: 50,
    backgroundColor: '#F0EEED',
    // position: 'absolute',
    // bottom: 0,
    // width: Dimensions.get('window').width,
    justifyContent: 'flex-end',
    // alignItems: 'flex-end',
    alignSelf: 'stretch',
  },
  submitButton: {
    height: 47,
    backgroundColor: '#1f6389',
    justifyContent: 'center',
    alignItems: 'center',
  },
  submitButtonText: {
    color: 'white',
  },
  friendList: {
    position: 'absolute',
    right: 10,
    top: 15,
  },
  rowBackView: {
    flex: 1,
    backgroundColor: 'red',
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingRight: 15,
  },
  rowBackText: {
    color: 'white',
  },
  keyboardView: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 20,
    paddingTop: 20,
  },
  centering: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 8,
  },
  noFriends: {
    fontSize: 30,
  },

});

export default styles;
