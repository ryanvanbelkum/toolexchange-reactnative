import { View, Text, ScrollView, TouchableOpacity } from 'react-native';
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { InputField, Form } from 'react-native-form-generator';
import { logoutUser, updateUserProfile, updateFirebasePassword } from '../login/login-actions';
import styles from './settings-style';

class SettingsClass extends PureComponent {

  constructor() {
    super();
    this.updateFirstName = this.handleInputChange.bind(this, 'firstName');
    this.updateLastName = this.handleInputChange.bind(this, 'lastName');
    this.updateStreetAddress = this.handleInputChange.bind(this, 'streetAddress');
    this.updateCityAddress = this.handleInputChange.bind(this, 'cityAddress');
    this.updateStateAddress = this.handleInputChange.bind(this, 'stateAddress');
    this.updateZipAddress = this.handleInputChange.bind(this, 'zipAddress');
    this.updateTelephone = this.handleInputChange.bind(this, 'telephone');
    this.logoutUser = this.logoutUser.bind(this);
    this.updateUser = this.updateUser.bind(this);
    this.jumpToLogin = this.jumpToLogin.bind(this);
  }

  componentDidMount() {
    this.setState({
      firstName: this.props.firstName,
      lastName: this.props.lastName,
      streetAddress: this.props.address.street,
      cityAddress: this.props.address.city,
      stateAddress: this.props.address.state,
      zipAddress: this.props.address.zip,
      telephone: this.props.telephone,
    });
  }

  handleInputChange(value, text) {
    this.setState({
      [value]: text,
    });
  }

  updateUser() {
    const { streetAddress, cityAddress, stateAddress, zipAddress, firstName, lastName, telephone } = this.state;
    const address = {
      street: streetAddress,
      city: cityAddress,
      state: stateAddress,
      zip: zipAddress,
    };

    this.props.updateUserProfile(firstName, lastName, this.props.userName, telephone, address);
  }

  logoutUser() {
    this.props.logoutUser(this.jumpToLogin);
  }

  jumpToLogin() {
    this.props.navigator.jumpTo(this.props.routes[0]);
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.box1}>
            <Text style={styles.headerText}>
              {this.props.title}
            </Text>
          </View>
          <Form style={styles.box2}>
            <InputField onValueChange={this.updateFirstName} placeholder={this.props.firstName} />
            <InputField onValueChange={this.updateLastName} placeholder={this.props.lastName} />
            <InputField autoCapitalize='none' onValueChange={this.updateStateAddress} placeholder={this.props.address.street || '<street>'} />
            <InputField autoCapitalize='none' onValueChange={this.updateCityAddress} placeholder={this.props.address.city || '<city>'} />
            <InputField autoCapitalize='none' onValueChange={this.updateStateAddress} placeholder={this.props.address.state || '<state>'} />
            <InputField autoCapitalize='none' onValueChange={this.updateZipAddress} placeholder={this.props.address.zip || '<zip>'} />
            <InputField onValueChange={this.updateTelephone} placeholder={this.props.telephone} />
            <TouchableOpacity style={[styles.submitButton, styles.resetPassword]} onPress={this.props.updateFirebasePassword}>
              <Text style={styles.submitButtonText}>
                SEND RESET PASSWORD EMAIL
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.submitButton} onPress={this.updateUser}>
              <Text style={styles.submitButtonText}>
                UPDATE
              </Text>
            </TouchableOpacity>
          </Form>
        </ScrollView>
        <TouchableOpacity style={styles.logout} onPress={this.logoutUser}>
          <Text style={styles.submitButtonText}>
            LOGOUT
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}


function mapStateToProps(state) {
  const login = state.login;
  return {
    firstName: login.firstName,
    lastName: login.lastName,
    username: login.username,
    telephone: login.telephone,
    email: login.email,
    address: login.address,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    logoutUser(navigationCallback) {
      dispatch(logoutUser(navigationCallback));
    },
    updateFirebasePassword() {
      dispatch(updateFirebasePassword());
    },
    updateUserProfile(firstName, lastName, username, telephone, email, address) {
      dispatch(updateUserProfile(firstName, lastName, username, telephone, email, address));
    },
  };
}

const Settings = connect(
  mapStateToProps,
  mapDispatchToProps,
)(SettingsClass);

export default Settings;
