import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  box: {

  },
  box1: {
    height: 60,
    backgroundColor: '#1f6389',
    justifyContent: 'center',
    alignItems: 'center'
  },
  headerText: {
    color: '#FFF',
    fontSize: 20,
    fontFamily: 'Iowan Old Style',
  },
  textInput: {
    height: 45,
    textAlign: 'center'
  },
  textInputView: {
    backgroundColor: '#FFF'
  },
  box2: {
    flex: 10,
    backgroundColor: '#F0EEED'
  },
  box3: {
    flex: 2,
    backgroundColor: '#F0EEED'
  },
  submitButton: {
    height: 50,
    backgroundColor: '#1f6389',
    justifyContent: 'center',
    alignItems: 'center'
  },
  submitButtonText: {
    color: 'white'
  },
  logout: {
    backgroundColor: '#1f6389',
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    backgroundColor: 'red'
  },
  resetPassword: {
    backgroundColor: '#97A0A2'
  }

});

export default styles;