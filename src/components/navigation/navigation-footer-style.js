import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  navIcon: {
    width: 30,
    height: 30,
  },
  navIconLeft: {
    marginLeft: 20,
  },
  navIconRight: {
    marginRight: 20,
  },
  currentTab: {
    tintColor: 'green',
  },
  box4: {
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#F6F6F4',
  },
});

export default styles;
