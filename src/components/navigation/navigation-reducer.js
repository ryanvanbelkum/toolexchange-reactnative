import React from 'react';
import { tabs } from '../../helpers.js';

const defaultState = {
  currentTab: tabs.LOGIN,
  toastVisable: false,
  toastMessage: '',
  index: 0
};

export default function navigation(state = defaultState, action) {
  switch (action.type) {
    case 'SHOW_LOGIN':
      return {
        ...state,
        currentTab: tabs.LOGIN,
        index: 0,
        title: 'Login'
      };
    case 'SHOW_PROFILE':
      return {
        ...state,
        currentTab: tabs.PROFILE,
        index: 3,
        title: 'Profile'
      };
    case 'SHOW_LOCAL_TOOLS':
      return {
        ...state,
        currentTab: tabs.LOCAL_TOOLS,
        index: 1,
        title: 'Friend Tools'
      };
    case 'SHOW_SETTINGS':
      return {
        ...state,
        currentTab: tabs.SETTINGS,
        index: 2,
        title: 'Settings'
      };
    case 'SHOW_ACTIVITY_INDICATOR':
      return {
        ...state,
        showActivityIndicator: true
      };
    case 'HIDE_ACTIVITY_INDICATOR':
      return {
        ...state,
        showActivityIndicator: false
      };
    case 'TOGGLE_TOAST':
      return {
        ...state,
        toastVisable: !state.toastVisable,
        toastMessage: action.toastMessage
      };
    default:
      return state;
  }
}
