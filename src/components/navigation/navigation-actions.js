export function showLogin() {
  return {
    type: 'SHOW_LOGIN'
  };
}

export function showProfile() {
  return {
    type: 'SHOW_PROFILE'
  };
}

export function showLocalTools() {
  return {
    type: 'SHOW_LOCAL_TOOLS'
  };
}

export function showSettings() {
  return {
    type: 'SHOW_SETTINGS'
  };
}

export function showActivityIndicator() {
  return {
    type: 'SHOW_ACTIVITY_INDICATOR'
  };
}

export function hideActivityIndicator() {
  return {
    type: 'HIDE_ACTIVITY_INDICATOR'
  };
}

export function toggleToast(message) {
  return {
    type: 'TOGGLE_TOAST',
    toastMessage: message
  };
}

export function displayToast(message) {
  return dispatch => {
    dispatch(toggleToast(message))
    setTimeout(() => {
      dispatch(toggleToast(''));
    }, 5000);
  }
}
