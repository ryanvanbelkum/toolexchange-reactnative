import { View, Image, TouchableOpacity } from 'react-native';
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import styles from './navigation-footer-style';
import { tabs } from '../../helpers';
import { showProfile, showLocalTools, showSettings } from './navigation-actions';

const personIcon = require('../../img/person-outline.png');
const toolIcon = require('../../img/tool-icon.png');
const settingsIcon = require('../../img/settings.png');

class NavFooterClass extends PureComponent {

  constructor() {
    super();
    this.transitionProfile = this.transitionScene.bind(this, tabs.PROFILE);
    this.transitionLocalTools = this.transitionScene.bind(this, tabs.LOCAL_TOOLS);
    this.transitionSettings = this.transitionScene.bind(this, tabs.SETTINGS);
  }

  transitionScene(tab) {
    if (this.props.currentTab !== tab) {
      switch (tab) {
        case tabs.PROFILE:
          this.props.showProfile();
          this.props.navigator.jumpTo(this.props.routes[3]);
          break;
        case tabs.LOCAL_TOOLS:
          this.props.showLocalTools();
          this.props.navigator.jumpTo(this.props.routes[1]);
          break;
        case tabs.SETTINGS:
          this.props.showSettings();
          this.props.navigator.jumpTo(this.props.routes[2]);
          break;
      }
    }
  }

  render() {
    let icon1Classes = [styles.navIcon, styles.navIconLeft];
    let icon2Classes = [styles.navIcon];
    let icon3Classes = [styles.navIcon, styles.navIconRight];
    if (this.props.currentTab === tabs.PROFILE) {
      icon1Classes = icon1Classes.concat(styles.currentTab);
    } else if (this.props.currentTab === tabs.LOCAL_TOOLS) {
      icon2Classes = icon2Classes.concat(styles.currentTab);
    } else if (this.props.currentTab === tabs.SETTINGS) {
      icon3Classes = icon3Classes.concat(styles.currentTab);
    }
    const returnView = this.props.currentTab === tabs.LOGIN ? null :
    (<View style={styles.box4}>
      <TouchableOpacity onPress={this.transitionProfile}>
        <Image style={icon1Classes} source={personIcon} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.transitionLocalTools}>
        <Image style={icon2Classes} source={toolIcon} />
      </TouchableOpacity>
      <TouchableOpacity onPress={this.transitionSettings}>
        <Image style={icon3Classes} source={settingsIcon} />
      </TouchableOpacity>
    </View>);
    return returnView;
  }
}

function mapStateToProps(state) {
  const navigation = state.navigation;
  return {
    currentTab: navigation.currentTab,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    showProfile() {
      dispatch(showProfile());
    },
    showLocalTools() {
      dispatch(showLocalTools());
    },
    showSettings() {
      dispatch(showSettings());
    },
  };
}

const NavFooter = connect(
  mapStateToProps,
  mapDispatchToProps,
)(NavFooterClass);


export default NavFooter;
