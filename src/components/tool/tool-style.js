import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    height: 70,
    borderBottomColor: '#D3D3D3',
    borderBottomWidth: 1,
    flexDirection: 'row',
    backgroundColor: '#FFF',
  },
  view: {
    flex: 1,
  },
  text: {
    paddingLeft: 10,
  },
  nameText: {
    fontSize: 25,
  },
  ownerText: {
  },
  grayBackground: {
    backgroundColor: '#F0EEED',
  },
  pinkBackgroun: {
    backgroundColor: '#F2D7D5',
  },
  base64: {
    height: 70,
    width: 70,
    alignItems: 'flex-end',
    borderRadius: 35,
  },
  toolFont: {
    fontFamily: 'Helvetica',
  },
  statusIcon: {
    height: 30,
    width: 30,
  },
  iconView: {
    justifyContent: 'center',
    paddingRight: 10,
  },

});

export default styles;
