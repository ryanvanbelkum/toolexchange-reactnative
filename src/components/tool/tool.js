import { View, Image, Text } from 'react-native';
import React, { PureComponent } from 'react';
import styles from './tool-style';

const availableIcon = require('../../img/check.png');
const checkedOutIcon = require('../../img/circlewithx.gif');

class Tool extends PureComponent {

  render() {
    const { name, description, image, borrower, owner, currentUser } = this.props;
    const statusIcon = borrower ? checkedOutIcon : availableIcon;
    const base64image = `data:image/png;base64,${image}`;
    let containerStyles = [styles.container];

    if (owner && owner !== currentUser) {
      containerStyles = containerStyles.concat(styles.pinkBackgroun);
    }

    return (
      <View style={containerStyles}>
        <View>
          <Image style={styles.base64} source={{ uri: base64image, scale: 3 }} />
        </View>
        <View style={styles.view}>
          <Text numberOfLines={1} style={[styles.text, styles.nameText]}>
            { name }
          </Text>
          <Text numberOfLines={2} style={styles.text}>
            { description }
          </Text>
        </View>
        <View style={styles.iconView}>
          <Image style={styles.statusIcon} source={statusIcon} />
        </View>
      </View>
    );
  }
}

Tool.propTypes = {
  description: React.PropTypes.string,
  image: React.PropTypes.string,
  tab: React.PropTypes.string,
  name: React.PropTypes.string,
  borrower: React.PropTypes.string,
  owner: React.PropTypes.string,
  currentUser: React.PropTypes.string
};

export default Tool;
