import { View, Text, TouchableHighlight, Modal, ScrollView, TouchableOpacity, Alert, Keyboard, Image } from 'react-native';
import { Form, Separator, InputField, LinkField, SwitchField, PickerField, DatePickerField, TimePickerField } from 'react-native-form-generator';
import styles from './add-new-tool-style.js';
import React, { PureComponent } from 'react';
import AddNewToolPhoto from './add-new-tool-photo.js';

const close = require('../../img/Cancel-58.png');

class AddNewTool extends PureComponent {

  constructor() {
    super();
    this.updateToolName = this.handleInputChange.bind(this, 'toolName');
    this.updateToolDescription = this.handleInputChange.bind(this, 'description');
    this.setNewToolPhoto = this.handleInputChange.bind(this, 'newPhotoImage');
    this.addToolToState = this.addToolToState.bind(this);

    this.state = {
      newPhotoImage: ''
    }
  }

  handleInputChange(value, text) {
    this.setState({
      [value]: text
    });
  }

  addToolToState() {
    this.props.addNewTool(this.state.toolName, this.state.description, this.state.newPhotoImage);
    this.setNewToolPhoto('');
    this._toolName.refs.fieldComponent.refs.inputBox.setNativeProps({
      text: ''
    });
    this._toolDescription.refs.fieldComponent.refs.inputBox.setNativeProps({
      text: ''
    });
    Keyboard.dismiss();
  }

  render() {
    return (
      <View style={ styles.container }>
        <Modal style={ styles.container }
          animationType={ "slide" }
          transparent={ false }
          visible={ this.props.modalOpen }
          onRequestClose={ () => {
                           } }>
          <View style={ styles.container }>
            <View style={ styles.box1 }>
              <TouchableOpacity style={ styles.closeButton } onPress={ this.props.toggleModal }>
                <Image style={ styles.closeIcon } source={ close } />
              </TouchableOpacity>
              <Text style={ styles.headerText }>
                Add new tool
              </Text>
            </View>
            <Form style={ styles.box2 }>
              <InputField onValueChange={ this.updateToolName } ref={ component => this._toolName = component } placeholder='Tool Name' />
              <InputField onValueChange={ this.updateToolDescription } ref={ component => this._toolDescription = component } placeholder='Description' />
              <AddNewToolPhoto setNewToolPhoto={ this.setNewToolPhoto } newPhotoImage={ this.state.newPhotoImage } />
              <TouchableOpacity style={ styles.submitButton } onPress={ this.addToolToState }>
                <Text style={ styles.submitButtonText }>
                  ADD TOOL
                </Text>
              </TouchableOpacity>
            </Form>
          </View>
        </Modal>
        <TouchableOpacity style={ styles.submitButton } onPress={ this.props.toggleModal }>
          <Text style={ styles.submitButtonText }>
            ADD NEW TOOL
          </Text>
        </TouchableOpacity>
      </View>
      );
  }
}
;

AddNewTool.propTypes = {
  toggleModal: React.PropTypes.func,
  addNewTool: React.PropTypes.func,
  modalOpen: React.PropTypes.bool
}

export default AddNewTool;
