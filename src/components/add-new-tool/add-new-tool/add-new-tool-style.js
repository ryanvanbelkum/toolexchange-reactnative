import { StyleSheet, Dimensions } from 'react-native';


const styles = StyleSheet.create({

  container: {
    flex: 1
  },
  box1: {
    height: 60,
    backgroundColor: '#1f6389',
    justifyContent: 'center',
    alignItems: 'center'
  },
  headerText: {
    color: '#FFF',
    fontSize: 20,
    fontFamily: 'Iowan Old Style',
  },
  textInput: {
    height: 45,
    textAlign: 'center'
  },
  textInputView: {
    backgroundColor: '#FFF'
  },
  box2: {
    flex: 10,
    backgroundColor: '#F0EEED'
  },
  submitButton: {
    height: 50,
    backgroundColor: '#1f6389',
    justifyContent: 'center',
    alignItems: 'center'
  },
  submitButtonText: {
    color: 'white'
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 10,
    margin: 40
  },
  launchCamera: {
    backgroundColor: '#97A0A2'
  },
  base64: {
    flex: 1,
    height: 50,
    resizeMode: 'contain'
  },
  closeIcon: {
    zIndex: 100,
    width: 30,
    height: 30
  },
  closeButton: {
    position: 'absolute',
    right: 10,
    top: 15,
    zIndex: 100,
    backgroundColor: 'white',
    borderRadius: 35,
  }
});


export default styles;
