'use strict';
import React, { Component } from 'react';
import { Text, TouchableHighlight, View, Image, TouchableOpacity } from 'react-native';
import Camera from 'react-native-camera';
import styles from './add-new-tool-style.js';
import RNFS from 'react-native-fs'

const camera = require('../../img/Camera.png');

class AddNewToolPhoto extends Component {

  render() {
    const base64image = `data:image/png;base64,${this.props.newPhotoImage}`
    const cameraVsImage = this.props.newPhotoImage ?
      <Image style={ styles.base64 } source={ { uri: base64image, scale: 3 } } /> :
      <Camera ref={ (cam) => {
                this.camera = cam;
              } }
        style={ styles.preview }
        captureTarget={ Camera.constants.CaptureTarget.disk }
        aspect={ Camera.constants.Aspect.fill }>
        <TouchableOpacity style={ styles.capture } onPress={ this.takePicture.bind(this) }>
          <Image style={ styles.closeIcon } source={ camera } />
        </TouchableOpacity>
      </Camera>

    return (
      <View style={ styles.container }>
        { cameraVsImage }
      </View>
      );
  }

  takePicture() {
    this.camera.capture()
      .then((data) => {
        RNFS.readFile(data.path, 'base64')
          .then(res => this.props.setNewToolPhoto(res));
      }
    ).catch(err => console.error(err));
  }
}

AddNewToolPhoto.propTypes = {
  setNewToolPhoto: React.PropTypes.func,
  newPhotoImage: React.PropTypes.string
}

export default AddNewToolPhoto;
