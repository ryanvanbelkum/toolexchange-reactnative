import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import Spinner from 'react-native-loading-spinner-overlay';

class ActivitySpinnerClass extends PureComponent {

  render() {
    return <Spinner visible={this.props.showActivityIndicator} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />;
  }
}

function mapStateToProps(state) {
  return {
    showActivityIndicator: state.navigation.showActivityIndicator,
  };
}

const ActivitySpinner = connect(
  mapStateToProps,
)(ActivitySpinnerClass);

export default ActivitySpinner;
