import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 10,
    paddingBottom: 20
  },
  modal: {
    flex: 1
  },
  addFriendIcon: {
    tintColor: 'white',
    height: 30,
    width: 30,
    alignItems: 'flex-end'
  },
  close: {
    backgroundColor: '#1f6389',
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    backgroundColor: 'red'
  },
  submitButtonText: {
    color: 'white'
  },
  searchBar: {
    paddingLeft: 30,
    fontSize: 22,
    height: 10,
    flex: .1,
    borderWidth: 9,
    borderColor: '#E4E4E4',
  },
  separator: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#8E8E8E',
  },

});

export default styles;


