import { View, Image, TouchableOpacity, Text, Modal, ListView } from 'react-native';
import React, { PureComponent } from 'react';
import styles from './friends-list-style';
import Header from './friends-list-header';
import SectionHeader from './friends-list-section-headers';
import FriendsListRow from './friends-list-row';

const addFriend = require('../../img/add_friend.png');

class FriendsList extends PureComponent {

  constructor(props) {
    super(props);
    this.setSearchText = this.setSearchText.bind(this);
    this.renderHeader = this.renderHeader.bind(this);
    this.state = {
      searchText: '',
    };
    const getSectionData = (dataBlob, sectionId) => dataBlob[sectionId];
    const getRowData = (dataBlob, sectionId, rowId) => dataBlob[`${rowId}`];
    this.ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2,
      sectionHeaderHasChanged: (s1, s2) => s1 !== s2,
      getSectionData,
      getRowData,
    });
  }

  setSearchText(event) {
    const searchText = event.nativeEvent.text;
    this.setState({
      searchText,
    });
  }

  filterUsers(searchText, users) {
    const text = searchText.toLowerCase();

    return users.filter((user) => {
      const firstName = user.firstName.toLowerCase();
      const lastName = user.lastName.toLowerCase();
      const nameToSearch = `${firstName} ${lastName}`;
      return nameToSearch.search(text) !== -1;
    });
  }

  formatData(data) {
    const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
    const dataBlob = {};
    const sectionIds = [];
    const rowIds = [];

    for (let sectionId = 0; sectionId < alphabet.length; sectionId += 1) {
      const currentChar = alphabet[sectionId];
      const users = data.filter(user => user.username.toUpperCase().indexOf(currentChar) === 0);

      if (users.length > 0) {
        sectionIds.push(sectionId);
        dataBlob[sectionId] = {
          character: currentChar,
        };
        rowIds.push([]);

        for (let i = 0; i < users.length; i += 1) {
          const rowId = `${sectionId}:${i}`;
          rowIds[rowIds.length - 1].push(rowId);
          dataBlob[rowId] = users[i];
        }
      }
    }

    return {
      dataBlob,
      sectionIds,
      rowIds,
    };
  }

  areWeFriends(user) {
    return this.props.friends.includes(user.username);
  }

  renderRow(data) {
    return (<FriendsListRow
      updateFriendsList={() => {
        this.props.updateFriendsList(data);
      }}data={data} areWeFriends={this.areWeFriends(data)}
    />);
  }

  renderSeparator(sectionId, rowId) {
    return <View key={rowId} style={styles.separator} />;
  }

  renderHeader() {
    return <Header searchChange={this.setSearchText} />;
  }

  renderSectionHeader(sectionData) {
    return <SectionHeader {...sectionData} />;
  }

  render() {
    const users = this.filterUsers(this.state.searchText, this.props.users);
    const { dataBlob, sectionIds, rowIds } = this.formatData(users);
    const dataSource = this.ds.cloneWithRowsAndSections(dataBlob, sectionIds, rowIds);

    return (
      <View>
        <Modal
          style={styles.modal}
          animationType={'slide'}
          transparent={false}
          visible={this.props.friendsListOpen}
          onRequestClose={() => {}}
        >
          <View style={styles.container}>
            <ListView
              style={styles.container}
              dataSource={dataSource}
              renderRow={data => this.renderRow(data)}
              renderSeparator={(sectionId, rowId) => this.renderSeparator(sectionId, rowId)}
              renderHeader={this.renderHeader}
              renderSectionHeader={sectionData => this.renderSectionHeader(sectionData)}
            />
          </View>
          <TouchableOpacity style={styles.close} onPress={this.props.toggleFriendsList}>
            <Text style={styles.submitButtonText}>
              CLOSE
            </Text>
          </TouchableOpacity>
        </Modal>
        <TouchableOpacity onPress={this.props.toggleFriendsList}>
          <Image style={styles.addFriendIcon} source={addFriend} />
        </TouchableOpacity>
      </View>
    );
  }
}


FriendsList.propTypes = {
  users: React.PropTypes.array,
  friends: React.PropTypes.array,
  toggleFriendsList: React.PropTypes.func,
  updateFriendsList: React.PropTypes.func,
  friendsListOpen: React.PropTypes.bool,
  loggedInUser: React.PropTypes.string
};

export default FriendsList;
