import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';

const friend = require('../../img/friend.png'),
  styles = StyleSheet.create({
    container: {
      flexDirection: 'row',
      padding: 10
    },
    text: {
      fontSize: 20,
      flex: 1
    },
    icon: {
      height: 20,
      width: 20
    }
  });

function showFriendIcon(areWeFriends) {
  return areWeFriends ? <Image style={ styles.icon } source={ friend } /> : null;
}

/*esfmt-ignore-start*/
const FriendsListRow = ({data, updateFriendsList, areWeFriends}) => (
  <TouchableOpacity onPress={ updateFriendsList }>
    <View style={ styles.container }>
      <Text style={ styles.text }>{ data.firstName } { data.lastName }</Text>
      { showFriendIcon(areWeFriends) }
    </View>
  </TouchableOpacity>
);
/*esfmt-ignore-end*/

export default FriendsListRow;