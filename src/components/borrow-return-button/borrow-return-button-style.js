import { StyleSheet, Dimensions } from 'react-native';

const styles = StyleSheet.create({

  borrowerIcon: {
    height: 30,
    width: 30,
    marginRight: 10
  },
  lineItem: {
    flexDirection: 'row',
  },
  borrowReturnTouch: {
    width: 150
  },
  borrowText: {
    color: '#007f00',
    fontSize: 20,
    padding: 3
  },
  returnText: {
    color: '#FF0000',
    fontSize: 20,
    padding: 3
  }
});

export default styles;
