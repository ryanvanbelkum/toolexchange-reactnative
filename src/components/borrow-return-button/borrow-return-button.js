import { View, Image, TouchableOpacity, Text } from 'react-native';
import React, { Component } from 'react';
import styles from './borrow-return-button-style.js';

const borrowArrowIcon = require('../../img/DoubleRight-58.png');
const returnArrowIcon = require('../../img/DoubleLeft-58.png');

const BorrowReturnButton = ({borrower, currentUser, returnTool, borrowTool, owner}) => {

  let borrowReturnView = null;
  if (borrower && borrower === currentUser) {
    borrowReturnView = <TouchableOpacity style={ styles.borrowReturnTouch } onPress={ returnTool }>
                         <View style={ styles.lineItem }>
                           <Image style={ styles.borrowerIcon } source={ returnArrowIcon } />
                           <Text style={ styles.returnText }>
                             RETURN NOW
                           </Text>
                         </View>
                       </TouchableOpacity>
  } else if (!borrower && owner !== currentUser) {
    borrowReturnView = <TouchableOpacity style={ styles.borrowReturnTouch } onPress={ borrowTool }>
                         <View style={ styles.lineItem }>
                           <Image style={ styles.borrowerIcon } source={ borrowArrowIcon } />
                           <Text style={ styles.borrowText }>
                             BORROW NOW
                           </Text>
                         </View>
                       </TouchableOpacity>;
  }

  return borrowReturnView;
}


BorrowReturnButton.propTypes = {
  borrower: React.PropTypes.string,
  currentUser: React.PropTypes.string,
  owner: React.PropTypes.string,
  returnTool: React.PropTypes.func,
  borrowTool: React.PropTypes.func,
}

export default BorrowReturnButton;
