import { View, TouchableOpacity, Text } from 'react-native';
import React, { PureComponent } from 'react';
import styles from './tool-card-style';
import Tool from '../tool/tool';

class ToolCard extends PureComponent {

  render() {
    const { tools, onPress } = this.props;
    const toolsInCard = Object.keys(tools).map(key =>
      <TouchableOpacity key={key} onPress={() => onPress(tools[key])}>
        <View style={styles.tool}>
          <Tool
            name={tools[key].name}
            description={tools[key].description}
            image={tools[key].image}
            borrower={tools[key].borrower}
          />
        </View>
      </TouchableOpacity>);
    const owner = tools[Object.keys(tools)[0]].owner;

    return (
      <View>
        <Text style={styles.ownerText}>
          { owner }
        </Text>
        <View activeOpacity={1} style={styles.container}>
          { toolsInCard }
        </View>
      </View>
    );
  }
}

ToolCard.propTypes = {
  //add shape
  tools: React.PropTypes.object,
  onPress: React.PropTypes.func
};

export default ToolCard;
