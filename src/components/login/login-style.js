import { StyleSheet, Dimensions } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,.6)'
  },
  textInput: {
    height: 70,
    textAlign: 'center',
    color: '#FFF'
  },
  textInputView: {
    backgroundColor: 'rgba(255,255,255,.5)',
    margin: 10
  },
  errorMessage: {
    textAlign: 'center',
    color: 'red',
    fontSize: 16
  },
  errorMessageView: {
    alignItems: 'center',
    marginTop: 50,
    backgroundColor: '#FFF'
  },
  buttonGroup: {
    paddingTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  backgroundImage: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'cover',
  },
  iconImage: {
    height: 100,
    width: 100
  },
  iconView: {
    marginTop: Dimensions.get('window').height / 5,
    alignItems: 'center',
    marginBottom: 40,
  },
  titleText: {
    color: '#FFF',
    fontSize: 28,
  },
  touchableButton: {
    alignItems: 'center',
    backgroundColor: 'rgba(255,183,50,.9)',
    width: Dimensions.get('window').width / 3,
    height: 40,
    justifyContent: 'center',
  },
  buttonText: {
    color: '#FFF',
    fontSize: 20,
  },
  checkBoxView: {
    margin: 10,
  },
  checkboxLable: {
    color: '#FFF',
    fontSize: 20,
  },
  checkboxIcon: {

  },
});

export default styles;
