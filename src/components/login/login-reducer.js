const defaultState = {
  errorMessage: '',
  firstName: '',
  lastName: '',
  username: '',
  telephone: '',
  email: '',
  address: {}
};

export default function login(state = defaultState, action) {
  switch (action.type) {
    case 'VALIDATE_FORM':
      return {
        ...state,
        errorMessage: action.errorMessage ? action.errorMessage : '',
        formValidation: action.formValidation
      };
    case 'SET_ERROR_MESSAGE':
      return {
        ...state,
        errorMessage: action.errorMessage ? action.errorMessage : '',
      };
    case 'SET_USER_BY_UID':
      return {
        ...state,
        loggedInUsername: action.loggedInUsername,
        loggedInUserUid: action.loggedInUserUid
      }
    case 'SET_USER_PROFILE':
      return {
        ...state,
        firstName: action.firstName,
        lastName: action.lastName,
        username: action.username,
        telephone: action.telephone,
        email: action.email,
        address: action.address
      }
    case 'ADD_FCM_TOKEN_TO_STATE':
      return {
        ...state,
        token: action.token
      }
    default:
      return state;
  }
}
