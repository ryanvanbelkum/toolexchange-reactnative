import { geoKey, rebase } from '../../helpers.js';
import Rebase from 're-base';
import { showLocalTools, showLogin, showActivityIndicator } from '../navigation/navigation-actions.js';
import Geocoder from 'react-native-geocoding';
import * as Keychain from 'react-native-keychain';

const base = Rebase.createClass(rebase);

export function setErrorMessage(msg) {
  return {
    type: 'SET_ERROR_MESSAGE',
    errorMessage: msg,
  };
}

function setUserByUid(uid, username) {
  return {
    type: 'SET_USER_BY_UID',
    loggedInUsername: username,
    loggedInUserUid: uid,
  };
}

export function setUserProfile(firstName, lastName, username, telephone, email, address, lat, lng) {
  return {
    type: 'SET_USER_PROFILE',
    firstName,
    lastName,
    username,
    telephone,
    email,
    address,
    lat,
    lng,
  };
}

export function addFCMTokenToState(token) {
  return {
    type: 'ADD_FCM_TOKEN_TO_STATE',
    token,
  };
}

// ASYNC TASKS
export function logoutUser(navigationCallback) {
  return (dispatch) => {
    base.unauth();
    dispatch(setUserByUid('', ''));
    dispatch(showLogin());
    dispatch(resetKeychain());
    navigationCallback();
  };
}

export function updateFirebasePassword() {
  return (dispatch, getState) => {
    const email = getState().login.email;
    base.resetPassword({
      email,
    }, () => {});
  };
}

export function loginUser(email, password, rememberMe, navigationCallbackSuccess, navigationCallbackFail) {
  return (dispatch) => {
    base.authWithPassword({
      email,
      password,
    }, (error, user) => {
      if (error) {
        dispatch(setErrorMessage(error.message));
        navigationCallbackFail();
      } else {
        dispatch(getUsername(user.uid));
        dispatch(setErrorMessage(''));
        if (rememberMe){
          dispatch(setKeychainCreds(email, password));
        }
        navigationCallbackSuccess();
      }
    });
  };
}

function setKeychainCreds(username, password) {
  return () => {
    Keychain
      .setGenericPassword(username, password)
      .then(() => {
        console.log('Credentials saved successfully!');
      });
  };
}

export function resetKeychain() {
  return () => {
    Keychain
      .resetGenericPassword()
      .then(() => {
        console.log('Credentials successfully deleted');
      });
  };
}

function createUserWithGeoLocation(address, createUserCallback) {
  return (dispatch) => {
    Geocoder.setApiKey(geoKey);
    Geocoder.getFromLocation(`${address.street}, ${address.city}, ${address.state} ${address.zip}`).then(
      (json) => {
        const location = json.results[0].geometry.location;
        createUserCallback(location.lat, location.lng);
      }).catch(() => {
        createUserCallback(' ', ' ');
      });
  };
}

function addUserToIndex() {
  return (dispatch, getState) => {
    const login = getState().login;
    base.post(`userIndex/${login.loggedInUserUid}`, {
      data: login.loggedInUsername
    });
  }
}

function getUsername(userUid) {
  return dispatch => {
    base.fetch(`userIndex/${userUid}`, {
      context: {},
      asArray: false
    }).then(data => {
      dispatch(setUserByUid(userUid, data));
      dispatch(getUserProfile(data));
    }).catch(error => {
      dispatch(setErrorMessage(error.message));
    })
  }
}

function getUserProfile(username) {
  return (dispatch) => {
    base.fetch(`garages/${username}/profile`, {
      context: {},
      asArray: false,
    }).then((data) => {
      dispatch(setUserProfile(data.firstName, data.lastName, data.username,
        data.telephone, data.email, data.address));
      dispatch(setFCMToken());
      dispatch(showLocalTools());
    }).catch((error) => {
      dispatch(setErrorMessage(error.message));
    });
  };
}

export function createNewUser(firstName, lastName, username, telephone, email, address, password, successCallback) {
  return (dispatch) => {
    dispatch(showActivityIndicator());
    base.auth().createUserWithEmailAndPassword(email, password).then((user) => {
      dispatch(createUserWithGeoLocation(address, (lat, lng) => {
        const newAddress = lat && lng ? {
          ...address,
          lat,
          lng,
        } : address;
        dispatch(setUserProfile(firstName, lastName, username, telephone, email, newAddress));
        dispatch(createUserProfileInFirebase());
        dispatch(setUserByUid(user.uid, username));
        dispatch(addUserToIndex());
        dispatch(setFCMToken());
        dispatch(showLocalTools());
        if (successCallback) {
          successCallback();
        }
      }));
    }, (error) => {
      dispatch(setErrorMessage(error.message));
    });
  };
}

export function updateUserProfile(firstName, lastName, username, telephone, address) {
  return (dispatch, getState) => {
    const state = getState().login;
    const newFirstName = firstName ? firstName : state.firstName || null;
    const newLastName = lastName ? lastName : state.lastName || null;
    const newUsername = username ? username : state.username || null;
    const newTelephone = telephone ? telephone : state.telephone || null;
    const newEmail = state.email;
    const newAddress = {
      ...address,
      ...state.address,
    };
    dispatch(createUserWithGeoLocation(newAddress, (lat, lng) => {
      const newerAddress = lat && lng ? {
        ...newAddress,
        lat,
        lng,
      } : address;
      dispatch(setUserProfile(newFirstName, newLastName, newUsername, newTelephone, newEmail, newerAddress));
      dispatch(createUserProfileInFirebase());
      dispatch(setFCMToken());
    }));
  };
}

function createUserProfileInFirebase() {
  return (dispatch, getState) => {
    const login = getState().login;
    base.post(`garages/${login.username}/profile`, {
      data: {
        firstName: login.firstName,
        lastName: login.lastName,
        username: login.username,
        telephone: login.telephone,
        email: login.email,
        address: login.address,
      },
    }).then(() => {

    }).catch((error) => {
      dispatch(setErrorMessage(error.message));
    });
  };
}

export function setFCMToken() {
  return (dispatch, getState) => {
    const login = getState().login;
    if (login.username && login.token) {
      base.post(`garages/${login.username}/profile/token`, {
        data: login.token
      }).then(() => {

      }).catch(error => {
        dispatch(setErrorMessage(error.message));
      });
    }
  }
}
