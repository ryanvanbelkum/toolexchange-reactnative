import React, { PureComponent } from 'react';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CheckBox from 'react-native-checkbox';
import { connect } from 'react-redux';
import { View, Text, TextInput, Image, TouchableOpacity } from 'react-native';
import styles from './login-style';
import { loginUser, createNewUser, setErrorMessage } from './login-actions';
import { showActivityIndicator } from '../navigation/navigation-actions';
import SignUp from '../sign-up/sign-up';

const background = require('../../img/toolsBackground.jpg');
const icon = require('../../img/Maintenance.png');
const checkedBox = require('../../img/checkedCheckboxFilled.png');
const uncheckedBox = require('../../img/unCheckedBox.png');

class LoginClass extends PureComponent {

  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
      showModal: false,
      rememberMe: false,
    };

    this.onButtonLoginPress = this.onButtonLoginPress.bind(this);
    this.jumpToToolList = this.jumpToToolList.bind(this);
    this.emailUpdate = this.handleInputChange.bind(this, 'email');
    this.passwordUpdate = this.handleInputChange.bind(this, 'password');
    this.toggleModal = this.toggleModal.bind(this);
    this.rememberMeToggled = this.rememberMeToggled.bind(this);
    this.loginFail = this.loginFail.bind(this);
  }

  onButtonLoginPress() {
    // if (this.state.email && this.state.password) {
    if (true) {
      this.props.displayActivityIndicator();
      if (this.state.email === '') {
        this.props.loginUser('ryanvanbelkum@gmail.com', 'ryanG00290929', this.state.rememberMe, this.jumpToToolList, this.loginFail);
      } else {
        this.props.loginUser(this.state.email, this.state.password, this.state.rememberMe, this.jumpToToolList, this.loginFail);
      }
    } else {
      this.props.setErrorMessage('Please enter your email and password');
    }
  }

  loginFail() {
    this.props.setErrorMessage('login failed :(');
  }

  jumpToToolList() {
    this.setState({
      showModal: false,
    });
    this.props.navigator.jumpTo(this.props.routes[1]);
  }

  handleInputChange(value, text) {
    this.setState({
      [value]: text,
    });
  }

  rememberMeToggled() {
    this.setState({
      rememberMe: !this.state.rememberMe,
    });
  }

  toggleModal() {
    this.setState({
      showModal: !this.state.showModal,
    });
  }

  render() {
    const error = this.props.errorMessage ?
      (<View style={styles.errorMessageView}>
        <Text style={styles.errorMessage}>
          {this.props.errorMessage}
        </Text>
      </View>) : null;

    return (
      <View style={styles.container}>
        <Image style={styles.backgroundImage} source={background}>
          <View style={styles.container}>
            <KeyboardAwareScrollView>
              <View style={styles.iconView}>
                <Image style={styles.iconImage} source={icon} />
                <Text style={styles.titleText}>
                  Tool Exchange
                </Text>
              </View>
              <View style={styles.textInputView}>
                <TextInput
                  autoCapitalize="none"
                  underlineColorAndroid="transparent"
                  style={styles.textInput}
                  placeholder="email"
                  placeholderTextColor="#FFF"
                  onChangeText={this.emailUpdate}
                />
              </View>
              <View style={styles.textInputView}>
                <TextInput
                  autoCapitalize="none"
                  underlineColorAndroid="transparent"
                  secureTextEntry
                  style={styles.textInput}
                  placeholder="password"
                  placeholderTextColor="#FFF"
                  onChangeText={this.passwordUpdate}
                />
              </View>
              <View style={styles.checkBoxView}>
                <CheckBox
                  label="Remember Me"
                  checked={this.state.rememberMe}
                  onChange={this.rememberMeToggled}
                  labelStyle={styles.checkboxLable}
                  checkboxStyle={styles.checkboxIcon}
                  underlayColor="transparent"
                  checkedImage={checkedBox}
                  uncheckedImage={uncheckedBox}
                />
              </View>
              <View style={styles.buttonGroup}>
                <TouchableOpacity style={styles.touchableButton} onPress={this.onButtonLoginPress}>
                  <Text style={styles.buttonText}>
                    Sign In
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.touchableButton} onPress={this.toggleModal}>
                  <Text style={styles.buttonText}>
                    Sign Up
                  </Text>
                </TouchableOpacity>
              </View>
              {error}
            </KeyboardAwareScrollView>
          </View>
        </Image>
        <SignUp
          showModal={this.state.showModal}
          toggleModal={this.toggleModal}
          createNewUser={this.props.createNewUser}
          checkForUsername={this.props.checkForUsername}
          successCallback={this.jumpToToolList}
        />
      </View>
    );
  }
}

function mapStateToProps(state) {
  const login = state.login;
  return {
    errorMessage: login.errorMessage,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    loginUser(username, password, rememberMe, callbackSuccess) {
      dispatch(loginUser(username, password, rememberMe, callbackSuccess));
    },
    createNewUser(firstName, lastName, username, telephone, email, address, password, successCallback) {
      dispatch(createNewUser(firstName, lastName, username, telephone, email, address, password, successCallback));
    },
    displayActivityIndicator() {
      dispatch(showActivityIndicator());
    },
    setErrorMessage(msg) {
      dispatch(setErrorMessage(msg));
    },
  };
}

const Login = connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginClass);

export default Login;
