import { View, Image, Text } from 'react-native';
import React, { Component } from 'react';
import styles from './tool-hero-image-style.js';

const ToolHeroImage = ({image, name}) => {

  const base64image = `data:image/png;base64,${image}`,
   imageSection = image ?
  <View style={ styles.image }>
    <Image style={ styles.base64 } source={ { uri: base64image } }>
      <View style={ styles.backdropView }>
        <Text style={ styles.headline }>
          { name }
        </Text>
      </View>
    </Image>
  </View> :
  <View style={ styles.image }>
    <Text style={ styles.headline }>
      { name }
    </Text>
  </View>;

  return imageSection;
}


ToolHeroImage.propTypes = {
  image: React.PropTypes.string,
  name: React.PropTypes.string
}

export default ToolHeroImage;
