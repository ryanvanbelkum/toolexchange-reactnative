import { StyleSheet, Dimensions } from 'react-native';

const styles = StyleSheet.create({

  image: {
    flex: 1,
    marginBottom: 10,
    marginTop: 20,
  },
  base64: {
    flex: 1,
    height: Dimensions.get('window').height / 2,
    width: Dimensions.get('window').width
  },
  headline: {
    fontSize: 22,
    textAlign: 'left',
    backgroundColor: 'rgba(0,0,0,0.5)',
    color: 'white'
  }
});

export default styles;
