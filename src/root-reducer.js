import { combineReducers } from 'redux';
import login from './components/login/login-reducer.js';
import toolList from './components/tool-list/tool-list-reducer.js';
import navigation from './components/navigation/navigation-reducer.js';


export default combineReducers({
  login,
  toolList,
  navigation
});
