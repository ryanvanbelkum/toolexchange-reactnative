import React, { Component } from 'react';
import { Platform } from 'react-native';
import FCM, { FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType } from 'react-native-fcm';


class PushController extends Component {

  componentDidMount() {
    FCM.requestPermissions(); // for iOS

    FCM.getFCMToken().then((token) => {
      this.props.addFCMTokenToState(token);
    });
    this.notificationListener = FCM.on(FCMEvent.Notification, async (notif) => {
      if (notif.local_notification) {
        // empty
      }
      if (notif.opened_from_tray) {
        // empty
      }
      // await () => {};

      if (Platform.OS === 'ios') {
        switch (notif._notificationType) {
          case NotificationType.Remote:
            notif.finish(RemoteNotificationResult.NewData);
            break;
          case NotificationType.NotificationResponse:
            notif.finish();
            break;
          case NotificationType.WillPresent:
            notif.finish(WillPresentNotificationResult.All);
            break;
          default:
            break;
        }
      }
    });
    this.refreshTokenListener = FCM.on(FCMEvent.RefreshToken, (token) => {
      this.props.addFCMTokenToState(token);
      this.props.setFCMToken();
    });
  }


  componentWillUnmount() {
    this.notificationListener.remove();
    this.refreshTokenListener.remove();
  }

  render() {
    return null;
  }
}

PushController.propTypes = {
  addFCMTokenToState: React.PropTypes.func.isRequired,
  setFCMToken: React.PropTypes.func.isRequired,
};

export default PushController;
