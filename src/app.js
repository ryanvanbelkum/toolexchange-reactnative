/* eslint import/no-unresolved: [2, { ignore: ['buildStyleInterpolator'] }]*/

import React, { PureComponent } from 'react';
import buildStyleInterpolator from 'buildStyleInterpolator';
import * as Keychain from 'react-native-keychain';
import { View, Navigator } from 'react-native';
import { connect } from 'react-redux';
import { addFCMTokenToState, setFCMToken, loginUser, resetKeychain, setErrorMessage } from './components/login/login-actions';
import Login from './components/login/login';
import Profile from './components/profile/profile';
import ToolList from './components/tool-list/tool-list';
import Settings from './components/settings/settings';
import PushController from './push-controller';
import { showActivityIndicator, hideActivityIndicator } from './components/navigation/navigation-actions';
import NavFooter from './components/navigation/navigation-footer';
import ActivitySpinner from './components/activity-spinner/activity-spinner';

const NoTransition = {
  opacity: {
    from: 1,
    to: 1,
    min: 1,
    max: 1,
    type: 'linear',
    extrapolate: false,
    round: 100,
  },
};
const Transitions = {
  NONE: {
    ...Navigator.SceneConfigs.FadeAndroid,
    gestures: null,
    defaultTransitionVelocity: 1000,
    animationInterpolators: {
      into: buildStyleInterpolator(NoTransition),
      out: buildStyleInterpolator(NoTransition),
    },
  },
};

class AppClass extends PureComponent {

  constructor() {
    super();
    this.keychainFail = this.keychainFail.bind(this);
    this.state = {
      routes: [
        {
          id: 'login',
        },
        {
          id: 'toollist',
        },
        {
          id: 'settings',
        },
        {
          id: 'profile',
        },
      ],
    };
    this.setInitialRoute = this.setInitialRoute.bind(this);
  }

  componentWillMount() {
    Keychain
      .getGenericPassword()
      .then((credentials) => {
        if (credentials && credentials.password && credentials.username) {
          this.props.displayActivityIndicator();
          this.props.loginUser(credentials.username, credentials.password, false, this.setInitialRoute.bind(this, 1), this.keychainFail);
        } else {
          this.keychainFail();
        }
      }).catch(() => {
        this.keychainFail();
      });
  }

  setInitialRoute(index) {
    this.setState({
      initialRoute: this.state.routes[index],
    });
  }

  keychainFail() {
    this.setInitialRoute(0);
    this.props.hideActivityIndicator();
  }

  renderScene(route, navigator) {
    switch (route.id) {
      case 'login':
        return (<Login navigator={navigator} routes={this.state.routes} title="Login" />);
      case 'toollist':
        return (<ToolList title="Tool List" />);
      case 'profile':
        return (<Profile title="Profile" />);
      case 'settings':
        return (<Settings navigator={navigator} routes={this.state.routes} title="Settings" />);
      default:
        return (<Login navigator={navigator} routes={this.state.routes} title="Login" />);
    }
  }

  render() {
    const view = this.state.initialRoute ?
      (<Navigator
        initialRoute={this.state.initialRoute}
        initialRouteStack={this.state.routes}
        configureScene={() => Transitions.NONE}
        renderScene={(route, navigator) =>
          <View style={{ flex: 1 }}>
            {this.renderScene(route, navigator)}
            <NavFooter navigator={navigator} routes={this.state.routes} />
          </View>}
      />) : null;
    return (
      <View style={{ flex: 1 }}>
        {view}
        <ActivitySpinner />
        <PushController addFCMTokenToState={this.props.addFCMTokenToState} setFCMToken={this.props.setFCMToken} />
      </View>
    );
  }
}

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return {
    addFCMTokenToState(token) {
      dispatch(addFCMTokenToState(token));
    },
    setFCMToken() {
      dispatch(setFCMToken());
    },
    loginUser(username, password, callbackSuccess, callbackFail) {
      dispatch(loginUser(username, password, callbackSuccess, callbackFail));
    },
    displayActivityIndicator() {
      dispatch(showActivityIndicator());
    },
    hideActivityIndicator() {
      dispatch(hideActivityIndicator());
    },
    setErrorMessage(message) {
      dispatch(setErrorMessage(message));
    },
    resetKeychain() {
      dispatch(resetKeychain());
    },
  };
}

const App = connect(
  mapStateToProps,
  mapDispatchToProps,
)(AppClass);

export default App;
