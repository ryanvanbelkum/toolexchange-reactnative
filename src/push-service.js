/* global Headers b:true*/
/* global fetch b:true*/

import { fcmKey } from './helpers';

const API_URL = 'https://fcm.googleapis.com/fcm/send';

class PushService {

  constructor() {
    this.sendData = this.sendData.bind(this);
    this.sendNotification = this.sendNotification.bind(this);
    this.sendNotificationWithData = this.sendNotificationWithData.bind(this);
  }

  sendNotification(token, title, message) {
    const body = {
      to: token,
      notification: {
        title,
        body: message,
        sound: 'default',
        click_action: 'fcm.ACTION.HELLO',
      },
      priority: 10,
    };

    this._send(JSON.stringify(body), 'notification');
  }

  sendData(token, title, message) {
    const body = {
      to: token,
      data: {
        title,
        body: message,
        sound: 'default',
        click_action: 'fcm.ACTION.HELLO',
        remote: true,
      },
      priority: 'normal',
    };

    this._send(JSON.stringify(body), 'data');
  }

  sendNotificationWithData(token, title, message) {
    const body = {
      to: token,
      notification: {
        title,
        body: message,
        sound: 'default',
        click_action: 'fcm.ACTION.HELLO',
      },
      data: {
        title,
        body: message,
        click_action: 'fcm.ACTION.HELLO',
        remote: true,
      },
      priority: 'high',
    };

    this._send(JSON.stringify(body), 'notification-data');
  }

  _send(body, type) {
    const headers = new Headers({
      'Content-Type': 'application/json',
      'Content-Length': parseInt(body.length, 10),
      Authorization: `key=${fcmKey}`,
    });

    fetch(API_URL, {
      method: 'POST',
      headers,
      body,
    })
      .then(response => console.log(`Send ${type} response ${response}`))
      .catch(error => console.log(`Error sending ${type}`, error));
  }

}

const pushService = new PushService();
export default pushService;
